from websocket import create_connection
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import os
import time

import unittest

class UserTest(unittest.TestCase):
    def test_ui(self):
        baseUrl = os.getenv('baseUrl', "localhost:3001")

        commandsEnv = os.getenv('commands', "http,/cloudshell,psm::ws,oci --help,Commands::ws,oci iam compartment list,SAMPLE::ws,oci network vcn list --compartment-id ocid1.compartment.oc1..xyz,AVAILABLE,Commands:ws,oci iam compartment list,ACTIVE")

        commands = commandsEnv.split("::")

        options = Options()
        options.headless = True

        driver = webdriver.Chrome(chrome_options=options)

        i = 0
        test_counter = 0

        pid = None
        ws = None

        debug = False

        for command_string in commands:
            if debug:
                print(":"+command_string+":")
            command_line = command_string.split(",")

            if command_line[0] == "http":
                driver.get("http://" + baseUrl + command_line[1])

            if command_line[0] == "ws":
                if pid is None:
                    r = requests.get("http://" + baseUrl + "/lastterminal")
                    pid = r.text
                    #print("pid of terminal is:" + str(pid))

                if ws is None:
                    if debug:
                        print("connecting to: "+ "ws://" + baseUrl + "/terminals/" + str(pid))
                    ws = create_connection("ws://" + baseUrl + "/terminals/" + str(pid))
                    time.sleep(2)
                    result = ""
                    while "/usr/src/app#" not in result:
                        result = result + ws.recv()
                    if debug:
                        print("Received '%s'" % result)
                    time.sleep(1)

                if debug:
                    print("sending to: " + "ws://" + baseUrl + "/terminals/" + str(pid))
                ws = create_connection("ws://" + baseUrl + "/terminals/" + str(pid))
                if debug:
                    print("sending: " + command_line[1] + "\n\n")
                ws.send(command_line[1] + "\n\n")
                result = ws.recv()
                while "/usr/src/app#" not in result:
                    result = result + ws.recv()
                if debug:
                    print("Received '%s'" % result)
                result = ""
                while "/usr/src/app#" not in result:
                    result = result + ws.recv()
                if debug:
                    print("Received '%s'" % result)

                if command_line[2] in result:
                    print("GOOD: Test Passed, " + command_line[2] + " found running :" + command_line[1])

                if command_line[2] not in result:
                    print("ERROR: Test Failed, " + command_line[2] + " not in :" + result)

                time.sleep(5)

            time.sleep(2)
            driver.save_screenshot("screenshots/test_"+str(i)+".png")
            i = i+1

        driver.close()

if __name__ == '__main__':
    unittest.main()
