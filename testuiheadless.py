from websocket import create_connection
import requests
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

import os
import time

import unittest

class UserTest(unittest.TestCase):
    def test_ui(self):
        baseUrl = os.getenv('baseUrl', "localhost:4000")

        commandsEnv = os.getenv('commands', "http,/#/,Dashboard::buttontext,Create App,Name::labelinput,Name,asampleapp::buttonclass,btn-primary,asampleapp::buttonclass,dropdown-toggle,::buttonclassalert,text-danger,Dashboard")

        commands = commandsEnv.split("::")

        options = Options()
        options.headless = False

        driver = webdriver.Chrome(chrome_options=options)
        driver.set_window_size(1920,1080)

        i = 0
        test_counter = 0

        pid = None
        ws = None

        debug = False

        for command_string in commands:
            if debug:
                print(":"+command_string+":")
            command_line = command_string.split(",")

            if command_line[0] == "http":
                driver.get("http://" + baseUrl + command_line[1])
                if debug:
                    print("checking " + command_line[2] + " in the following page:\n" +  driver.page_source)
                self.assertTrue(command_line[2] in driver.page_source)

            if command_line[0] == "linktext":
                link = driver.find_element_by_link_text('"'+command_line[1] + '"')
                link.click()
                if debug:
                    print("checking " + command_line[2] + " in the following page:\n" +  driver.page_source)
                self.assertTrue(command_line[2] in driver.page_source)

            if command_line[0] == "buttontext":
                button = driver.find_elements_by_xpath('//*[contains(text(), "' + command_line[1] + '")]')
                button[0].click()
                time.sleep(2)
                if debug:
                    print("checking " + command_line[2] + " in the following page:\n" +  driver.page_source)
                self.assertTrue(command_line[2] in driver.page_source)

            if command_line[0] == "buttonclass":
                button = driver.find_elements_by_class_name(command_line[1])
                button[0].click()
                time.sleep(2)
                if debug:
                    print("checking " + command_line[2] + " in the following page:\n" +  driver.page_source)
                self.assertTrue(command_line[2] in driver.page_source)

            if command_line[0] == "labelinput":
                textinput = driver.find_elements_by_xpath('//label[contains(text(), "' + command_line[1] + '")]/following-sibling::div//*')
                textinput[0].send_keys(command_line[2])

            if command_line[0] == "buttonclassalert":
                button = driver.find_elements_by_class_name(command_line[1])
                button[0].click()
                time.sleep(2)
                obj = driver.switch_to.alert
                obj.accept()
                if debug:
                    print("checking " + command_line[2] + " in the following page:\n" +  driver.page_source)
                self.assertTrue(command_line[2] in driver.page_source)

            time.sleep(2)
            driver.save_screenshot("screenshots/test_"+str(i)+".png")
            i = i+1

        driver.close()

if __name__ == '__main__':
    unittest.main()
