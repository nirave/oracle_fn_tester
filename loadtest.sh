#!/bin/bash

CONFIG=$1
if [ -z ${CONFIG} ]; then echo "Configuration File required"; fi

if [ -z ${LINK_IMAGE} ]; then LINK_IMAGE="mockcli2"; fi

if [ ! -e $CONFIG ]; then echo "Configuration File not found"; fi

BASEURL=`grep baseUrl ${CONFIG} | cut -f2- -d":" | xargs`
if [[ -v BASEURL ]]; then BASEURL="-e baseUrl=\"${BASEURL}\""; fi

NUMUSERS=`grep numUsers ${CONFIG} | cut -f2 -d":" | xargs`
if [[ -v NUMUSERS ]]; then NUMUSERS="-e numUsers=\"${NUMUSERS}\""; fi

RAMPUP=`grep rampUpTime ${CONFIG} | cut -f2 -d":" | xargs`
if [[ -v RAMPUP ]]; then RAMPUP="-e rampUpTime=\"${RAMPUP}\""; fi

COMMANDS=`grep -A1000 -m1 -e 'COMMANDS' $CONFIG | tail -n+2 | sed ':a;N;$!ba;s/\n/::/g'`
if [[ -v COMMANDS ]]; then COMMANDS="-e commands=\"${COMMANDS}\""; fi

LINK_IMAGE=`grep linkImage ${CONFIG} | cut -f2- -d":" | xargs`
if [ -z ${LINK_IMAGE} ]; then LINK_IMAGE="mockcli2"; fi

HOST_IP=`ip -4 addr show scope global dev docker0 | grep inet | awk '{print \$2}' | cut -d / -f 1`

set -x

eval "docker run --add-host dockerhost:$HOST_IP ${NUMUSERS} ${RAMPUP} ${BASEURL} ${COMMANDS} -v "$PWD/results":/opt/gatling/results loadfntester"

#eval "docker run --add-host dockerhost:$HOST_IP  ${NUMUSERS} ${RAMPUP} ${BASEURL} -v "$PWD/results":/opt/gatling/results loadfntester"
