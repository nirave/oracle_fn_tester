# oracle_fn_tester

Runs docker UI and load tests against OracleFn

This runs a docker image against OracleFn to run quick UI and load tests.

All it requires is docker and bash - and you can test away!

## To build:
cd uitestdocker
docker build -t uifntester .
cd loadtestdocker
docker build -t loadfntester .

## To run the uitest:
./uitest.sh uiconfig

screenshots will be in the screenshots directory

## To run the uitest:
./loadtest.sh loadconfigsample

Gattling results will be in the results directory

## Config files

This is where the magic happens.  UI configuraiton is simple.

Place the baseurl.  Note that dockerhost is used as the hostname because the assumption is that the docker host will contain OracleFn.  This is easily alterable in the uitest.sh

Next, list "Commands" by line.  Each line being comma separated by the action (e.g. http is go to a website, buttontext clicks a button), what to enter, and what text to check for.  That's it.  Adding more actions is trivial by altering uitestdocker/testuiheadless.py.

Similarly, loadconfigsample should be altered.  There is a similar to the UI configuration.  There is a baseurl, the number of users, and the ramp-up-time for all users.

In additions, the "Commands" are the API path to go to, a comma, and what regular expresison to check for.  More complex changes can be done at loadtestdocker/gatling/user-files/simulations/basic/test.scala

