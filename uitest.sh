#!/bin/bash
source /home/nirave/dev/cloudshell/vnenv/bin/activate

CONFIG=$1
if [ -z ${CONFIG} ]; then echo "Configuration File required"; fi

if [ ! -e $CONFIG ]; then echo "Configuration File not found"; fi

BASEURL=`grep baseUrl ${CONFIG} | cut -f2- -d":" | xargs`
if [[ -v BASEURL ]]; then BASEURL="-e baseUrl=\"${BASEURL}\""; fi

COMMANDS=`grep -A1000 -m1 -e 'COMMANDS' $CONFIG | tail -n+2 | sed ':a;N;$!ba;s/\n/::/g'`
if [[ -v COMMANDS ]]; then COMMANDS="-e commands=\"${COMMANDS}\""; fi

LINK_IMAGE=`grep linkImage ${CONFIG} | cut -f2- -d":" | xargs`
if [ -z ${LINK_IMAGE} ]; then LINK_IMAGE="mockcli2:v2"; fi

HOST_IP=`ip -4 addr show scope global dev docker0 | grep inet | awk '{print \$2}' | cut -d / -f 1`

#python testui.py
set -x

eval "docker run ${BASEURL} ${COMMANDS} -v "$PWD/screenshots":/screenshots --add-host dockerhost:$HOST_IP --shm-size=256m uifntester python /tmp/testuiheadless.py"

