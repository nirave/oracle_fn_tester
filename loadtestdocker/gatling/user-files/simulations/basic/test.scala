package computerdatabase 

import io.gatling.core.Predef._ 
import io.gatling.http.Predef._
import scala.concurrent.duration._

class BasicSimulation extends Simulation { 

  val baseUrl = scala.util.Properties.envOrElse("baseUrl", "localhost:4000" )

  val numUsers = scala.util.Properties.envOrElse("numUsers", "1" ).toInt

  val rampUpTime = scala.util.Properties.envOrElse("rampUpTime", "30" ).toInt

  val commandsText = scala.util.Properties.envOrElse("commands", "/#,.*Functions UI.*::/api/stats,.*Apps.*" )

  val commandLines = commandsText.split("::").map(_.trim)

  val commands = Array.ofDim[String](commandLines.length, 2)

  for(i <- 0 until commandLines.length) {
	val thisCommand = commandLines(i).split(",").map(_.trim)
	commands(i)(0) = thisCommand(0)
	commands(i)(1) = thisCommand(1)
	println ("Will run this command: " + commands(i)(0) + ", and check for this: " + commands(i)(1))
  }

  val httpConf = http
    .baseUrl("http://"+baseUrl)
    .userAgentHeader("Gatling3")

  val scn = scenario("BasicSimulation") 

	.repeat(commandLines.length, "counterName") {
	  exec(session => {
		  val id = session("counterName").as[Int]
		  session.set("the_command", commands(id)(0)).set("theCheck", commands(id)(1))
	  })
	  .exec(http("${the_command}")
	  .get("${the_command}")
	  .check(status is 200)
	  .check(regex("${theCheck}").find(0).exists)
	  .check(bodyString.saveAs("BODY")))
	  .pause(10)

	  //Uncomment below to print debug
	  /*
	  .exec(session => {
	    val response = session("BODY").as[String]
    	println(s"Response body: \n$response")
        session
      })

	  .exec { session => println(session); session }
	  */
	  
	}

	if (numUsers==1) {
      setUp( 
        scn.inject(
	    //Uncomment below if 1 user
	    atOnceUsers(1))
	    ).protocols(httpConf) 
    } else {
      setUp( 
        scn.inject(
	    rampUsers(numUsers) during (rampUpTime seconds))
      ).protocols(httpConf)
    }
}

